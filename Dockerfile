FROM alpine:3.4
VOLUME /tmp
ADD /target/*jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]