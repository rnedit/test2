package kz.test.testTests.controllers;

import kz.test.testTests.domain.Logs;
import kz.test.testTests.domain.ShopAlmaty;
import kz.test.testTests.repos.RepoLogs;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.util.*;

@Controller
@RequestMapping("/")
public class OneController {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(OneController.class);

    @Autowired
    RepoLogs repoLogs;

    @Autowired
    private RestTemplate restTemplate;

    private Integer totalPage;
    private Integer total;
    private final Integer perPage = 12;
    private ArrayList<ShopAlmaty> shopAlmaties = new ArrayList<>();
    private final String UPLOAD_DIR = "/tmp/testTest/";
    private String[] columns = {"title", "address", "time", "tel"};

    @GetMapping()
    public String index() {
        return "index";
    }


    @GetMapping("readWriteXLSX")
    public String readeWriteXLSX( Model model ) {

        final String FAKE_FILE = "/tmp/fake/fake_address.xlsx";
        final String API_URL = "http://www.mapquestapi.com/geocoding/v1/address?key=I50qIaOw13RLdN0oGjnAQOJs83HYS389&location=%s,%s,%s,%s";
        try {
            XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(FAKE_FILE));
            XSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);

            Iterator<Row> rowIterator = myExcelSheet.iterator();
            Row row = rowIterator.next();
            Cell latLngCell = row.createCell(4);
            latLngCell.setCellValue("latLng");
            while(rowIterator.hasNext()){
                row = rowIterator.next();
                String Country = row.getCell(0).getStringCellValue();
                String City = row.getCell(1).getStringCellValue();
                String Street = row.getCell(2).getStringCellValue();
                String Home = row.getCell(3).getStringCellValue();

                String url = String.format(API_URL,Country,City,Street,Home);

                HttpHeaders headers = new HttpHeaders();
                headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
                ResponseEntity<String> resultString = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
                JsonParser springParser = JsonParserFactory.getJsonParser();
                Map<String, Object> map = springParser.parseMap(resultString.getBody());
                Map<String, Object> info = (Map<String, Object>) map.get("info");

                if ((int)info.get("statuscode")==0) {
                    ArrayList results = (ArrayList) map.get("results");
                    Map<String, Object> one = (Map<String, Object>) results.get(0);
                    ArrayList locations = (ArrayList) one.get("locations");
                    Map<String, Object> firstLocation =  (Map<String, Object>) locations.get(0);
                    Map<String, Object> latLng = (Map<String, Object>) firstLocation.get("latLng");
                    latLngCell = row.getCell(4);
                    latLngCell.setCellValue(latLng.get("lat")+", "+latLng.get("lng"));
                } else {
                    System.out.println("Error "+Street+" "+Home+" "+info.get("statuscode"));
                }
                break; //для теста
            }
            myExcelSheet.autoSizeColumn(1);
            myExcelBook.write(new FileOutputStream(FAKE_FILE));
            myExcelBook.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        model.addAttribute("readWriteXLSX","1");
        return "index";
    }

    @GetMapping("shopAlmaty")
    public String getShop( Model model ) {

        final String URL =  "https://2gis.kz/almaty/search/продуктовые магазины в Алматы/page/%s";

        File file = new File(UPLOAD_DIR);
        file.mkdirs();
        Document docWeb = getDoc(URL,"1");
        Elements miniCard__headerTitleLink = getMiniCard(docWeb);
        setArrayCard(miniCard__headerTitleLink);

        for (int i = 2;i<getTotalPage();i++) {
            docWeb = getDoc(URL,String.valueOf(i));
            if (docWeb!=null) {
                miniCard__headerTitleLink = getMiniCard(docWeb);
                if (miniCard__headerTitleLink!=null) {
                    setArrayCard(miniCard__headerTitleLink);
                }
            }
            //break; //тест
            log.info("готово страниц "+i);
        }



        model.addAttribute("magazAlmaty","1");
        // model.addAttribute("countShop",shopAlmaties.size());
        model.addAttribute("totalPages",getTotalPage());
        model.addAttribute("total",getTotal());
        return "index";
    }

    private void setArrayCard(Elements miniCard__headerTitleLink) {
        Document docWeb;

        final String URL_SITE = "https://2gis.kz";
        for (Element el : miniCard__headerTitleLink) {
            try {
                docWeb = Jsoup.connect(URL_SITE+el.attr("href"))
                        .ignoreContentType(true)
                        .ignoreHttpErrors(true)
                        .validateTLSCertificates(false)
                        .userAgent("Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.9.1.4) Gecko/20091016 Firefox/3.5.4")
                        .timeout(10000)
                        .referrer("http://google.com")
                        .get();
                Element frame = docWeb.select("div[class$=frame _num_1 _state_visible _pos_center _moverDir_right _active _ready _cont_card]").first();

                if (frame!=null) {
                    Element tmp;
                    ShopAlmaty shopAlmaty = new ShopAlmaty();

                    tmp =frame.select("h1[class$=cardHeader__headerNameText]").first();
                    String cardHeader__headerNameText = tmp!=null? tmp.text() : "Не указано"  ;
                    shopAlmaty.setTitle(cardHeader__headerNameText);
//
                    tmp = frame.select("a[class$=card__addressLink _undashed]").first();
                    String card__addressLink = tmp!=null? tmp.text() : "Не указано"  ;

                    tmp =  frame.select("div[class$=_purpose_drilldown cardFeaturesItem]").first();
                    String _purpose_drilldown  = tmp!=null? tmp.text() : "Не указано"  ;

                    shopAlmaty.setAddress(
                            card__addressLink +
                                    ", " +
                                    _purpose_drilldown
                    );
//
                    tmp = frame.select("div[class$=microSchedule__title]").first();
                    String microSchedule__title  = tmp!=null? tmp.text() : "Время работы не указано"  ;
                    shopAlmaty.setTime(microSchedule__title);
//
                    tmp = frame.select("div[class$=contact__phonesVisible]").first();
                    String contact__phonesVisible  = tmp!=null? tmp.text() : "Телефон не указан"  ;

                    tmp = frame.select("div[class$=contact__phonesItemComment").first();
                    //if (tmp!=null) {
                    String contact__phonesItemComment = tmp!=null? tmp.text() : ""  ;
                    // }


                    shopAlmaty.setTel(
                            contact__phonesVisible +
                                    ", " +
                                    contact__phonesItemComment

                    );
//

                    shopAlmaties.add(shopAlmaty);
                    log.info("добавил "+shopAlmaty.getTitle());
                }
                // break; //тест
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        try {
            File f = new File(UPLOAD_DIR+"test.xlsx");
            XSSFSheet myExcelSheet;
            Row row;
            int rowNum = 1;
            XSSFWorkbook myExcelBook;
            if(f.exists() && !f.isDirectory()) {
                myExcelBook = new XSSFWorkbook(new FileInputStream(UPLOAD_DIR+"test.xlsx"));
                myExcelSheet = myExcelBook.getSheetAt(0);
                rowNum = myExcelSheet.getLastRowNum();
            } else {
                myExcelBook = new XSSFWorkbook();
                myExcelSheet = myExcelBook.createSheet("Almaty shop");
                row = myExcelSheet.createRow(0);
                for(int i = 0; i < columns.length; i++) {
                    Cell cell = row.createCell(i);
                    cell.setCellValue(columns[i]);
                }

                for(int i = 0; i < columns.length; i++) {
                    myExcelSheet.autoSizeColumn(i);
                }
            }
            for (ShopAlmaty sa: shopAlmaties) {
                row = myExcelSheet.createRow(rowNum++);
                row.createCell(0)
                        .setCellValue(sa.getTitle());
                row.createCell(1)
                        .setCellValue(sa.getAddress());
                row.createCell(2)
                        .setCellValue(sa.getTime());
                row.createCell(3)
                        .setCellValue(sa.getTel());
            }

            myExcelBook.write(new FileOutputStream(UPLOAD_DIR+"test.xlsx"));
            // myExcelBook.write(fileOut);
            // fileOut.close();
            myExcelBook.close();
            shopAlmaties.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Elements getMiniCard(Document docWeb) {
        Elements searchResults__list = docWeb.getElementsByClass("searchResults__list");
        String totalSearchResults__headerName = docWeb.getElementsByClass("searchResults__headerName").first().text();
        String total=totalSearchResults__headerName.replaceAll("\\D+","");
        int totalPages = Integer.valueOf(total)/perPage;
        setTotalPage(totalPages);
        setTotal(Integer.valueOf(total));
        Elements miniCards = searchResults__list.select("article[data-module$=miniCard]");
        Elements miniCard__headerTitleLink = miniCards.select("a[class$=link miniCard__headerTitleLink]");
        return miniCard__headerTitleLink;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }


    private Double getPageCount(Integer total, Integer perPage){
        Double totalPages = Math.ceil(Integer.valueOf(total)/perPage);
        return totalPages;
    }

    private Document getDoc(String URL,String page){
        Document docWeb = null;
        try {
            String url = String.format(URL, page);
            docWeb = Jsoup.connect(url)
                    .ignoreContentType(true)
                    .ignoreHttpErrors(true)
                    .validateTLSCertificates(false)
                    .userAgent("Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.9.1.4) Gecko/20091016 Firefox/3.5.4")
                    .timeout(10000)
                    .referrer("http://google.com")
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return docWeb;
    }

    @ResponseBody
    @PostMapping("3_cats")
    public Object cats( @NotNull @RequestParam String title,
                        @NotNull @RequestParam String img,
                        HttpServletRequest request) {
        final Map<String, Object> messageObject = new HashMap<>();

        Boolean result = false;


        final String UPLOAD_DIR = "/tmp/testTest/"+title+"/";
        File file = new File(UPLOAD_DIR);
        if(file.mkdirs()){
            try (FileOutputStream fos = new FileOutputStream(UPLOAD_DIR+title+".jpeg")) {
                fos.write(Base64.getDecoder().decode(img));
                result = true;
                messageObject.put("ask","Success");
            } catch (IOException e) {
                e.printStackTrace();
                messageObject.put("ask","IOException Error");
            }
        } else {
            messageObject.put("ask","Create Dir Error");
        }

        Logs logsRequest = new Logs(request.getRemoteAddr(), result);
        repoLogs.save(logsRequest);

        return messageObject;
    }

   /* @ResponseBody
    @PostMapping(value={"3_cats"}, produces = "application/json")
    public Object cats( @NotNull @RequestBody Object json,
                          HttpServletRequest request) {
        final Map<String, Object> messageObject = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.valueToTree(json);

        String title = actualObj.get("title").textValue();
        String img = actualObj.get("img").textValue();
        Boolean result = false;

        if ((title!=null && !title.isEmpty()) & (img!=null && !img.isEmpty())) {
            final String UPLOAD_DIR = "/tmp/testTest/"+title+"/";
            File file = new File(UPLOAD_DIR);
            if(file.mkdirs()){
                try (FileOutputStream fos = new FileOutputStream(UPLOAD_DIR+title+".jpeg")) {
                    fos.write(Base64.getDecoder().decode(img));
                    result = true;
                    messageObject.put("ask","Success");
                } catch (IOException e) {
                    e.printStackTrace();
                    messageObject.put("ask","IOException Error");
                }
            } else {
                messageObject.put("ask","Create Dir Error");
            }
        } else {
            messageObject.put("ask", "Empty Error");
        }

        Logs logsRequest = new Logs(request.getRemoteAddr(), result);
        repoLogs.save(logsRequest);

        return messageObject;
    }

    */
}
