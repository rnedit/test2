package kz.test.testTests.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "`Logs`")
public class Logs {

    public Logs(String remoteAddr, Boolean aTrue){
        this.setIp(remoteAddr);
        this.setResult(aTrue);
    }

    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="timestump",nullable = false,
            columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date timestamp = new Date();

    @Column(name="ip",nullable = false)
    private String ip;

    @Column(name="result",nullable = false)
    private Boolean result;

    public Integer getId() {
        return id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }
}
