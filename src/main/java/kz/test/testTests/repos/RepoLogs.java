package kz.test.testTests.repos;

import kz.test.testTests.domain.Logs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoLogs extends JpaRepository<Logs, Long> {
}
