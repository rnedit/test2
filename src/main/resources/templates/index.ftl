<form action="/3_cats" method="post">
    <br>
    title <input name="title"/>
    <br>
    img <input name="img">
    <br>
    <button type="submit">ok</button>
</form>

<form action="/readWriteXLSX" method="get">
    <br>
    Запись lat lng в fake_address.xlsx
    <button type="submit">го</button>
    <#if readWriteXLSX?? >
        <br>
        Что-то записали...
    </#if>
</form>

<form action="/shopAlmaty" method="get">
    <br>
    Все продуктовые магазины Алматы в xlsx
    <button type="submit">го</button>
    <#if magazAlmaty?? >
        <br>
        Что-то записали...
        <br>
        Всего найдено: ${total!"0"}
        <br>
        <!--
        Всего в массиве: ${countShop!"0"}
        <br>
        Всего страниц: ${totalPages!"0"}
        -->
    </#if>
</form>